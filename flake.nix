{
  description = "Nix flake wrapper around keycloak";
  inputs = {
     nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
     flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs,  flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        keycloak-src = pkgs.fetchzip {
            url = "https://github.com/keycloak/keycloak/releases/download/21.1.1/keycloak-21.1.1.tar.gz";
            hash = "sha256-ZX5UKjU9BEtO/uA25WhSUmeO6jQ1FpKF1irFx2fwPBU=";
        };
        keycloak-build-script = ''
          export JAVA_HOME=${pkgs.graalvm17-ce}
          cp lib/lib/main/org.keycloak.keycloak-themes-21.1.1.jar providers
          mkdir -p data/password-blacklists
          mkdir -p data/tmp
          echo "$(tail -n +2 bin/kc.sh)" >> bin/kc.new.sh
          chmod +x bin/kc.new.sh
          bin/kc.new.sh build --db dev-file
        '';
        keycloak-dev-build = pkgs.stdenv.mkDerivation {
          name = "keycloak-dev-build";
          src = keycloak-src;
          buildPhase = keycloak-build-script;
          installPhase = ''
          cp -r . $out
          '';
        };
        dev-server = pkgs.writeShellApplication {
          name = "custom-server";
          runtimeInputs = [pkgs.graalvm17-ce keycloak-dev-build];
          text = "export keycloak=${keycloak-dev-build}\n" + (builtins.readFile ./dev-server.sh);
        };
        server = pkgs.writeShellApplication {
          name = "keycloak";
          runtimeInputs = [pkgs.graalvm17-ce keycloak-src];
          text = "export keycloak=${keycloak-dev-build}\n" + (builtins.readFile ./server.sh);

        };

      in
        {
          packages = {
            default = server;
          };
        }
      );
}
