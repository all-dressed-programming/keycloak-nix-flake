
resolved_keycloak_data_dir="${KEYCLOAK_DATA_DIR:-$HOME/.keycloak}"
mkdir -p "$resolved_keycloak_data_dir"
exec java -Dkc.config.built=true \
          -Xms64m \
          -Xmx512m \
          -XX:MetaspaceSize=96M \
          -XX:MaxMetaspaceSize=256m \
          -Djava.net.preferIPv4Stack=true \
          -Dfile.encoding=UTF-8 \
          -Dsun.stdout.encoding=UTF-8 \
          -Dsun.err.encoding=UTF-8 \
          -Dstdout.encoding=UTF-8 \
          -Dstderr.encoding=UTF-8 \
          --add-opens=java.base/java.util=ALL-UNNAMED \
          --add-opens=java.base/java.util.concurrent=ALL-UNNAMED \
          --add-opens=java.base/java.security=ALL-UNNAMED \
          -Dkc.home.dir="$resolved_keycloak_data_dir" \
          -Djboss.server.config.dir="${keycloak}/conf" \
          -Djava.util.logging.manager=org.jboss.logmanager.LogManager \
          -Dquarkus-log-max-startup-records=10000 \
          -cp "${keycloak}/lib/quarkus-run.jar" \
          io.quarkus.bootstrap.runner.QuarkusEntryPoint \
          "$@" \
